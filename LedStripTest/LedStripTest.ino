// LedStripTest
// For Pendulum Clock project
// dylanhrush.com
// This sketch tests both your rotary encoder and 
// LED display. A single LED will light up, and
// as you rotate the axle, the LED will move across
// the strip.

// This optional setting causes Encoder to use more optimized code,
// It must be defined before Encoder.h is included.
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>

Encoder knob(2, 3);

/*Port Definitions*/
int Max7219_pinCLK = 7;
int Max7219_pinCS = 6;
int Max7219_pinDIN = 5;
 
unsigned char disp1[1024][8];
 
void Write_Max7219_byte(unsigned char DATA) {
  unsigned char i;
  digitalWrite(Max7219_pinCS,LOW);		
  for(i=8;i>=1;i--) {		  
    digitalWrite(Max7219_pinCLK,LOW);
    digitalWrite(Max7219_pinDIN,DATA&0x80);// Extracting a bit data
    DATA = DATA<<1;
    digitalWrite(Max7219_pinCLK,HIGH);
    }                                 
}
 
 
void Write_Max7219(unsigned char address,unsigned char dat) {
  digitalWrite(Max7219_pinCS,LOW);
  Write_Max7219_byte(address);           //address，code of LED
  Write_Max7219_byte(dat);               //data，figure on LED 
  digitalWrite(Max7219_pinCS,HIGH);
}
 
void Init_MAX7219(void) {
 Write_Max7219(0x09, 0x00);       //decoding ：BCD
 Write_Max7219(0x0a, 0x0F);       //brightness 
 Write_Max7219(0x0b, 0x07);       //scanlimit；8 LEDs
 Write_Max7219(0x0c, 0x01);       //power-down mode：0，normal mode：1
 Write_Max7219(0x0f, 0x00);       //test display：1；EOT，display：0
}
void setup()
{
  Serial.begin(9600);
  Serial.println("Hello world");
  pinMode(Max7219_pinCLK,OUTPUT);
  pinMode(Max7219_pinCS,OUTPUT);
  pinMode(Max7219_pinDIN,OUTPUT);
  delay(50);
  Init_MAX7219();
}
 
 
void loop()
{
   Serial.println(knob.read());
   writebit((knob.read()/8) % 64);
   delay(1000);	
}

void writebit(int j) {
   int seg=j/8;
   int pin=(1<<7) >>(j%8);
   for(unsigned char i=1;i<9;i++) {
      Write_Max7219(i,i==seg+1?pin:0);
   }
}
