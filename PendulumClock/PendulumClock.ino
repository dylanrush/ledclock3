// Dylan Rush
// LED Pendulum Clock Arduino Sketch
// blog.dylanhrush.com
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>


//Uncomment to enable Serial debugging
//#define DEBUG
#define pi 3.141592653589793

#define TICKS_REV  4096
#define POLAR_RES  128
#define LEDS 64
// Number of LEDs between axis and first LED
#define r1 20.923F

#define byte_hi_bit  0x80

/*Port Definitions*/
#define Max7219_pinCLK 7
#define Max7219_pinCS  6
#define Max7219_pinDIN  5

#define ROT_BLACK  2
#define ROT_WHITE 3

// clock face radius in LEDs
#define clock_radius  32

// minute hand length in LEDs
#define minute_hand_len  30

// hour hand length in LEDs
#define hour_hand_len  16

// how many cycles before the clock is considered settled
#define settle_threshold 15000

int knob_0;
int knob_last;
int settle_count;
boolean dark;

Encoder knob(ROT_BLACK, ROT_WHITE);

// This array saves the image in polar coordinates. To
// save memory, eight bytes are used to represent 64 
// LEDs.
unsigned char polar[POLAR_RES][8];

int getPolarX(int32_t tick) {
  int ticks = tick - knob_0;
  
  // 4 - spread is 1/4 of circle
  int polarX = POLAR_RES/2 + ticks * 4 * POLAR_RES / TICKS_REV;
  if (polarX < 0 || polarX >= POLAR_RES) {
    return -1;
  }
  return polarX;
}
void blankPolar() {
  for(int x=0; x<POLAR_RES; x++) {
    for(int y=0; y<8; y++) {
      polar[x][y] = 0x00;
    }
  }
}

// Activates the pixel in the polar[][] array
void writePolar(int polarX, int polarY) {
  int yByte = polarY/8;
  int yDig = polarY % 8;
  unsigned char a = polar[polarX][yByte];
  unsigned char b = byte_hi_bit >> yDig;
  polar[polarX][yByte] = a | b;
}

// Translates cartesian coordinates to polar coordinates
// and activates the pixel in the polar[][] array
void writeCartesian(float cartX, float cartY) {
  #ifdef DEBUG
  //Serial.print("cartesian[");
  //Serial.print(cartX);
  //Serial.print(",");
  //Serial.print(cartY);
  //Serial.println("]");
  #endif
  float triangleX = cartX;
  float triangleY = r1 + 32 - cartY;
  float theta = atan(triangleX/triangleY);
  // spread - data spans +- pi/4
  int polarX = 2*theta * POLAR_RES / pi + POLAR_RES/2;
  int polarY = triangleY / cos(theta) - r1;
  #ifdef DEBUG
  //Serial.print("polar[");
  //Serial.print(polarX);
  //Serial.print(",");
  //Serial.print(polarY);
  //Serial.println("]");
  #endif
  if (polarX < 0 || polarX >= POLAR_RES)
    return;
  
  if (polarY < 0 || polarY >= 64) 
    return;
  writePolar(polarX, polarY);
}

void drawClockFace() {
  for (float theta = 0.0; theta < pi/2; theta += pi/1024) {
    writeCartesian(clock_radius * cos(theta),
      clock_radius * sin(theta));
    writeCartesian(-clock_radius * cos(theta),
      clock_radius * sin(theta));
    writeCartesian(clock_radius * cos(theta),
      -clock_radius * sin(theta));
    writeCartesian(-clock_radius * cos(theta),
      -clock_radius * sin(theta));
  }
}

void drawHand(int radius, float theta) {
  for (float r=0; r<radius; r+=0.1F) {
    writeCartesian(r * cos(theta), r * sin(theta));
  }
}

// hour from 0-23
// minute from 0-59
// sec 0-59
void drawHands(int thour, int tminute, int sec) {
  int twhour = thour % 12;
  float theta = (pi / 2.0) - (2.0 * pi * ((float)twhour+tminute/60.0)/12.0);
  drawHand(hour_hand_len, theta);
  theta = (pi / 2.0) - (2.0 * pi * ((float)tminute+sec/60.0)/60.0);
  drawHand(minute_hand_len, theta);
}

void drawClock(int thour, int tminute, int sec) {  
  blankPolar();
  drawClockFace();
  #ifdef DEBUG
  Serial.println("Hands");
  #endif
  drawHands(thour, tminute, sec);
  #ifdef DEBUG
  Serial.println("Done");
  #endif
}

void Write_Max7219_byte(unsigned char DATA) 
{   
  unsigned char i;
  digitalWrite(Max7219_pinCS,LOW);		
  for(i=8;i>=1;i--)
  {		  
     digitalWrite(Max7219_pinCLK,LOW);
     digitalWrite(Max7219_pinDIN,DATA&0x80);// Extracting a bit data
     DATA = DATA<<1;
     digitalWrite(Max7219_pinCLK,HIGH);
   }                                 
}
 
 
void Write_Max7219(unsigned char address,unsigned char dat)
{
  digitalWrite(Max7219_pinCS,LOW);
  Write_Max7219_byte(address);           //address，code of LED
  Write_Max7219_byte(dat);               //data，figure on LED 
  digitalWrite(Max7219_pinCS,HIGH);
}
 
void Init_MAX7219(void)
{
 Write_Max7219(0x09, 0x00);       //decoding ：BCD
 Write_Max7219(0x0a, 0x0F);       //brightness 
 Write_Max7219(0x0b, 0x07);       //scanlimit；8 LEDs
 Write_Max7219(0x0c, 0x01);       //power-down mode：0，normal mode：1
 Write_Max7219(0x0f, 0x00);       //test display：1；EOT，display：0
}
 
void setup()
{
  #ifdef DEBUG
  Serial.begin(9600);
  Serial.println("Hello world");
  #endif
  // artificially rotate the encoder 
  // assume encoder is at the down position
  knob_last = TICKS_REV*2;
  knob.write(knob_last);
  pinMode(Max7219_pinCLK,OUTPUT);
  pinMode(Max7219_pinCS,OUTPUT);
  pinMode(Max7219_pinDIN,OUTPUT);
  delay(50);
  Init_MAX7219();
  
  drawClock();
  reset();
}
 
void reset() {
  settle_count = 0;
  dark = false;
  knob_0 = knob_last;
  
}

void drawClock() {
  tmElements_t tm;

  if (RTC.read(tm)) {
    drawClock(tm.Hour, tm.Minute, tm.Second);
  }
  

  else {
#ifdef DEBUG
    if (RTC.chipPresent()) {
      Serial.println("The DS1307 is stopped.  Please run the SetTime");
      Serial.println("example to initialize the time and begin running.");
      Serial.println();
    } else {
      Serial.println("DS1307 read error!  Please check the circuitry.");
      Serial.println();
    }
#endif
    drawClock(23, 45, 0);
  }

}
 
void loop()
{ 
  // This main control loop updates the image, resets the home
  // position of the encoder, or turns off the LEDs when the
  // clock is not turning.
  int knob_this = knob.read();
  if(knob_last == knob_this) {
    if(!dark && ++settle_count >= settle_threshold) {
      blankLeds();
      dark = true;
    }
  }
  else if (dark) {
    reset();
  }
  knob_last = knob_this;
  if(dark) {
    drawClock();
    return;
  }
  int polarX = getPolarX(knob_this);
  if(polarX != -1) {
    drawLeds(polarX);
  }
  else {
    blankLeds();
  }
}

void blankLeds() {
   for(unsigned char i=1;i<9;i++) {
      Write_Max7219(i,0);
   }
}

void drawLeds(int polarX) {
   for(unsigned char i=1;i<9;i++) {
      Write_Max7219(i,polar[polarX][i-1]);
   }
}
